+++
title = "About"
description = "Initial project description"
date = "2020-03-19"
author = "Nick Gerakines"
sec = 1
+++

Tavern is an Activity Pub service written in Golang.

The goal is to create a fast, stable, low-maintenance, and scalable Activity Pub service that can be deployed anyway by anyone.

The current release is [Amber Ale](https://gitlab.com/ngerakines/tavern/-/merge_requests/1).

Screenshots and development progress: [https://gitlab.com/ngerakines/tavern/-/wikis/Amber-Ale](https://gitlab.com/ngerakines/tavern/-/wikis/Amber-Ale)

